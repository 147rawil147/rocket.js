# Rocket.js
Данный класс написан для работы с api Rocketchat (restapi, livechat api)

### Установка
- Клонировать репозиторий 
`git clone https://147rawil147@bitbucket.org/147rawil147/rocket.js.git`
- Установить в ваш проект js-sha256
`npm install js-sha256`
- Подключить класс Rocket.js

### Инициализация
```javascript
    // Создаем объект класса
    // Первый параметр wss сервер чата
    // Второй параметр логин
    // Третий параметр пароль
    const rocket = new Rocket("wss://chat.trivers.ru/websocket", "rawil", "зфыыцщкв");

    // // Ждем авторизации и результата авторизации
    let logined = await rocket.checkLogined()
    
    // // Проверка на авторизацию
    // // Только после данной спешной провеки можно вызывать большинство методов класса!!!
    if(logined) {
      // Например тут мы подписываемся на комнату
      rocket.subscribe('GENERAL')
    }
```

## Важные замечания
- В документации описаны назания методов. Их нужно всегда вызывать в контексте объекта Rocketchat. В примерах можно найти как это делается
- Возможно глюки с методами комнат и каналов. rocketchat (не класс) имеет походие методы для каналов и комнат из-за этого при вызове метода комнаты для канала могут возникать ошибки. Рекомендую следовать документации и не думать об этом до тех пор пока не вылезет ошибка. В случае ошибка писать мне.

## Описание методов
#### Методы пользоватей

##### Метод отправляет запрос на авторизацию.

```javascript
logining(string login, string password)
// return undefined
```
Является методом livechat api. Ответ приходит на websocket. Проверить успешность авторизации можно методом `checkLogined`

------------
##### Возращает промис с юзером по его id
```javascript
getUser(string userId)
// return promise
```
###### Пример
```javascript
let r = await rocket.getUser('id')
console.log(r)
```

------------
##### Возращает промис с группой юзеров по массиву их id
```javascript
getUsers(array userId)
// return promise
```
###### Пример
```javascript
let ids = ['id1','id2']
let r = await rocket.getUsers(ids)
console.log(r)
```
------------
##### Возращает автар юзера по его id
```javascript
getAvatar(string userId)
// return string
```

------------
##### Создает юзера
Нужно быть авторизованным под админским аккаунтом
```javascript
createUser(object {
    email: "test@maila.com",
    name: "testname",
    password: "password",
    username: "username"
})
// return promise
```

------------
#### Методы связанные с комнатами и каналами
##### Возвращает историю канала
loadHistory(string id)

Возращает promise
###### Пример
```javascript
rocket.loadHistory(id).then(r => {
    console.log(r)
}
```
------------
##### Отправляет сообщение в комнату или канал
```javascript
createMsg(string msg, string roomId, string file=false)
// return undefined
```
Вложения пока не работают. Третий параметр не стоит передавать вообще. В будущих версиях будет исправленно.
Метод является методом livechat api response приходит в websocket
###### Пример
```javascript
rocket.createMsg("test", "GENERAL")
```

------------
######  Возращает промис с ответом от сервера в котором содержится канал
Один из этих параметров не обязателен. Они взаимоисключают друг-друга

```javascript
getChannel(string name, string id)
// return promise
```

###### Пример
```javascript
rocket.getChannel(false, "GENERAL").then(r => {
  console.log(r)
  // r это не только данные о канале, а полный ответ сервера
  // Канал хранится тут data.channel
})
```
------------
######  Возращает промис с ответом от сервера в котором содержится комната
Один из этих параметров не обязателен. Они взаимоисключают друг-друга
```javascript
getRoom(string name, string id)
// return promise
```


###### Пример
```javascript
rocket.getRoom("general").then(r => {
  console.log(r)
  // r это не только данные о канале, а полный ответ сервера
})
```
------------
#### Методы связанные с сообщениями
###### Подписывает авторизированного юзера на канал или комнату
```javascript
subscribe(string roomId)
```
После вызого данной функции при отправке сообщения в чат любым ее участником, в websocket гененрируется событие. Повесить callback на событие можно при помощи метода newMsgCallback

###### Пример
```javascript
rocket.subscribe('GENERAL')
rocket.newMsgCallback = msg => {
    // При отправке сообщения в чат любым участником данный callback будет срабатывать
    console.log(msg)
}
```
------------
###### Данный метод вызывается при отправке сообщения в чат любым участником
```javascript
newMsgCallback(r)
```
Изначально он задан так 
```javascript
// this - это обращение класса самого в себе
this.newMsgCallback = r => {}
```
Пример с ним приведен в методе описании метода subscribe
------------
###### Метод дополняет данные сообщениий
```javascript
updateMsgs(array msgs)
return msgs
```
Рекомендуется вызываеть его для всех сообщений. 
Данных с сервера не всегда бывает достаточно для использования сообщений и их приходится дополнять и изменять
###### Пример
```javascript
rocket.newMsgCallback = r => {
    // r.fields.args тут храянятся сообщения с сервера
    let msgs = rocket.updateMsgs(r.fields.args)
}
------------

